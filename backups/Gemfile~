source 'https://rubygems.org'
ruby '2.1.5'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.0.13'

# Use sqlite3 as the database for Active Record
# gem 'sqlite3'

gem 'pg'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.2'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano', group: :development

group :development do
  gem "better_errors"
  gem "pry-rails"
end

group :test do
  gem 'capybara'
  gem 'generator_spec', '~> 0.8'
  gem 'factory_girl_rails', '~>4.2'
  gem 'ffaker'
  gem 'rspec-rails', '~> 2.13'
  gem 'selenium-webdriver'
  gem 'database_cleaner'
end

# Use debugger
# gem 'debugger', group: [:development, :test]

# Core Spree gems
gem 'spree', github: 'spree/spree', branch: '2-1-stable'
gem 'spree_gateway', github: 'spree/spree_gateway', branch: '2-1-stable'
gem 'spree_auth_devise', github: 'spree/spree_auth_devise', branch: '2-1-stable'

# Extensions
gem 'spree_bootstrap_frontend', github: 'wohnstore/spree_bootstrap_frontend', branch: '2-1-stable'
gem 'spree_tax_cloud', github: 'jetsgit/spree_tax_cloud', branch: '2-1-stable'
gem 'spree_product_zoom', github: 'michaelmichael/spree_product_zoom', branch: '2-1-stable'
# gem 'spree_product_zoom', path: '../spree_product_zoom', branch: 'add-title'
# gem 'spree_call_for_quote', path: '../spree_call_for_quote', branch: '2-1-stable'
gem 'spree_active_shipping', github: 'spree/spree_active_shipping', branch: '2-1-stable'
gem 'spree_volume_pricing', github: 'spree/spree_volume_pricing', branch: '2-1-stable'
gem 'spree_static_content', github: 'spree/spree_static_content', branch: '2-1-stable'
gem 'spree_variant_options', github: 'BRZInc/spree_variant_options', branch: '2-1-stable'
gem 'spree_related_products', github: 'spree-contrib/spree_related_products', branch: '2-1-stable'
gem 'spree_call_for_quote', github: 'michaelmichael/spree_call_for_quote', branch: '2-1-stable'
# gem 'spree_call_for_quote', path: '../spree_call_for_quote', branch: '2-1-stable'

gem 'font-awesome-rails'
gem 'newrelic_rpm'
gem 'braintree'
gem 'haml-rails'
gem 'unicorn'
gem 'non-stupid-digest-assets'
gem 'datashift'
gem 'datashift_spree'
gem 'rack-cache'
