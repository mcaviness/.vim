# Configure Spree Preferences
#
# Note: Initializing preferences available within the Admin will overwrite any changes that were made through the user interface when you restart.
#       If you would like users to be able to update a setting with the Admin it should NOT be set here.
#
# In order to initialize a setting do:
# config.setting_name = 'new value'
require 'spree/core/product_filters'

Spree.config do |config|
  # Example:
  # Uncomment to override the default site name.
  config.site_name = "TCW Equipment Store"
  config.logo = 'store/tcw_logo_corner.png'
  config.max_level_in_taxons_menu = 5
  config.allow_guest_checkout = true
  config.company = true
end

Spree.config do |config|
  attachment_config = {
    s3_credentials: {
      access_key_id: ENV.fetch("s3_access_key"),
      secret_access_key: ENV.fetch("s3_secret"),
      bucket: ENV.fetch("s3_bucket"),
    },

    storage: :s3,
    s3_headers: { "Cache-Control" => "max-age=31557600" },
    s3_protocol: "https",
    bucket: ENV.fetch("s3_bucket"),

    styles: {
      mini: "48x48>",
      small: "100x100>",
      product: "240x240>",
      large: "600x600>"
    },

    path: "/spree/products/:id/:style/:basename.:extension",
    default_url: "/spree/products/:id/:style/:basename.:extension",
    default_style: "product",
  }

  attachment_config.each do |key, value|
    Spree::Image.attachment_definitions[:attachment][key.to_sym] = value
  end
end unless Rails.env.test?

Spree.user_class = "Spree::User"
