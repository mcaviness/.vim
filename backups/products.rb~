require_relative "./option_types.rb"
require_relative "./option_values.rb"

shipping_category = Spree::ShippingCategory.find_or_create_by!(name: "Default")

default_attrs = {
  :available_on => Time.zone.now,
  :shipping_category => shipping_category,
}

products = [
  {
    name: "Plastic 3-tool Wall Bracket",
    price: 24.98,
    description: "Storage of cleaning equipment ensures good hygiene practice, extends shelf life of brushware and promotes \"clean as you go\" principles. Polyproylene wall backet with rubber edging. Holds 3 products. Fully colour coded; visually promotes prevention of cross contamination. Smooth and easy to clean."
  },
  {
    name: "Stainless Steel Wall Bracket",
    price: 15.14,
    description: "Storage of cleaning equipment ensures good hygiene practice, extends shelf life of brushware and promotes \"clean as you go\" principles. Robust, stainless steel construction. Ultra hygienic with no crevices for bacteria buildup; easy to clean. Unique design hold all products.",
  },
  {
    name: "Wall Bracket System, 4-6 tools",
    price: 52.64,
    description: "Storage of cleaning equipment ensures good hygiene practice and extends shelf life of equipment. Fully colour coded. You can extend the bracket by adding one more next to the first bracket. You can mix rubber clips and hooks. Easy to clean, when you dismount the hooks and rubber clips - These parts can be sterilized in an autoclave."
  },
  {
    name: "Stainless Steel Table & Floor Scraper",
    price: 30.34,
    description: "Used to scrape and loosen stubborn, sticky dirt from tiled and concrete floors. Removing such dirt before scrubbing makes the cleaning process quicker. Also used on tables to scrape large surfaces."
  }
  {
    name: "Plastic Table & Floor Scraper",
    price: 33.17,
    description: "Used to scrape and loosen stubborn, sticky dirt from tiled and concrete floors. Removing such dirt before scrubbing makes the cleaning process quicker. Also used on tables to scrape large surfaces.",
    sku: "070A-29125"
  },
  {
    name: "Angle Cut Broom Head - Stiff Bristles",
    price: 27.98,
    description: "The angled design of this broom makes it easy to reach into narrow spaces between equipment when sweeping debris before the scrubbing action starts. It is particularly used for collecting large particles of dirt and debris in a moist environment."
  },
  {
    name: "Broom Head - Stiff Bristles",
    price: 36.34,
    description: "This heavy duty broom has long thick filaments which makes it ideal for sweeping heavy particles. This brush is also suitable for use in wet and external areas."
  },
  {
    name: "Resin-Set Broom Head",
    price: 73.26,
    description: "Used in hi-risk areas where concern for foreign body contamination from bristles is very high.",
  },
  {
    name: "6.5\" Mini Handle",
    price: 7.95,
    description: "A small, versatile handle. When paired with brush heads can be used to access tough-to-reach areas, or used with scrapers to get the last remaining product out."
  }
]

products.each do |product_attrs|
  default_shipping_category = Spree::ShippingCategory.find_by_name("Default")
  product = Spree::Product.create!(default_attrs.merge(product_attrs))
  product.reload
  product.shipping_category = default_shipping_category
  product.save!
end

blue = Spree::OptionValue.find_by_name("Blue")
red = Spree::OptionValue.find_by_name("Red")
green = Spree::OptionValue.find_by_name("Green")
yellow = Spree::OptionValue.find_by_name("Yellow")
white = Spree::OptionValue.find_by_name("White")
pink = Spree::OptionValue.find_by_name("Pink")
black = Spree::OptionValue.find_by_name("Black")
one_tool = Spree::OptionValue.find_by_name("1-tool")
two_tool = Spree::OptionValue.find_by_name("2-tool")
four_tool = Spree::OptionValue.find_by_name("4-tool")

variants = [
  {
    product: Spree::Product.find_by_name("Plastic 3-tool Wall Bracket"),
    option_values: [red],
    sku: "070A-06154"
  },
  {
    product: Spree::Product.find_by_name("Plastic 3-tool Wall Bracket"),
    option_values: [white],
    sku: "070A-06155"
  },
  {
    product: Spree::Product.find_by_name("Plastic 3-tool Wall Bracket"),
    option_values: [yellow],
    sku: "070A-06156"
  },
  {
    product: Spree::Product.find_by_name("Plastic 3-tool Wall Bracket"),
    option_values: [black],
    sku: "070A-06159"
  },
  {
    product: Spree::Product.find_by_name("Stainless Steel Wall Bracket"),
    option_values: [one_tool],
    sku: "070A-0616",
    price: 15.14
  },
  {
    product: Spree::Product.find_by_name("Stainless Steel Wall Bracket"),
    option_values: [two_tool],
    sku: "070A-0617",
    price: 37.36
  },
  {
    product: Spree::Product.find_by_name("Stainless Steel Wall Bracket"),
    option_values: [four_tool],
    sku: "070A-0618",
    price: 45.99
  }
]
Spree::Variant.create!(variants)

def dimensions_to_inches(dimensions)
  names = %w{height width depth}
  dimensions = dimensions.split.keep_if {|n| Float(n) != nil rescue false}
  Hash[names.zip(dimensions)]
end
