filetype off
"
" Basic options
set nocompatible " No vi compatibility
set visualbell " Silence the bell
set incsearch
set ruler
set laststatus=2 " Always show the statusline

" Use Vundle
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" Colors and Fonts
colorscheme earthburn
let &t_Co=256
set gfn=Mensch\ for\ Powerline:h13.00

" Change the freaking leader to comma
let mapleader=","
let g:Tex_leader=','

" Formatting
autocmd filetype css setlocal equalprg=csstidy\ -\ --silent=true
set expandtab " Expands tabs to spaces
set lines=50 columns=120
set wrap linebreak textwidth=0
set tabstop=2 shiftwidth=2 softtabstop=2
set linespace=3
set autoindent
set backspace=indent,eol,start
set ignorecase
set wildmenu
set smarttab
set number
set guioptions-=T "remove toolbar

Bundle 'gmarik/vundle'

" Bundles for Vundle
Bundle 'Lokaltog/vim-powerline'
Bundle 'tpope/vim-rails.git'
Bundle 'git://git.wincent.com/command-t.git'
Bundle 'L9'
Bundle 'FuzzyFinder'
Bundle 'tpope/vim-fugitive'
Bundle 'tpope/vim-rails.git'
Bundle 'scrooloose/nerdtree'
Bundle 'scrooloose/syntastic'
Bundle 'vim-ruby/vim-ruby'
Bundle 'tpope/vim-surround'
Bundle 'MarcWeber/vim-addon-mw-utils' 
Bundle 'drmingdrmer/xptemplate'
Bundle 'ervandew/supertab'
Bundle 'godlygeek/tabular'
Bundle 'henrik/vim-ruby-runner'

" avoid key conflict
let g:SuperTabMappingForward = '<Plug>supertabKey'

" if nothing matched in xpt, try supertab
let g:xptemplate_fallback = '<Plug>supertabKey'

" xpt uses <Tab> as trigger key
let g:xptemplate_key = '<Tab>'

" Filetype options
filetype plugin indent on
syntax on

" Use ack instead of grep
set grepprg=ack

" Powerline options
let g:Powerline_symbols='fancy'

" Use Preview to view .tex files
let g:tex_flavor='latex'
let g:Tex_ViewRule_pdf = 'Preview'
let g:Tex_DefaultTargetFormat="pdf" 


" Use arduino syntax highlighting
autocmd! BufNewFile,BufRead *.pde setlocal ft=arduino

let NERDSpaceDelims=1

" Map fuzzy file finder
nmap <c-h> :FufHelp<CR>
nmap <c-k> :FufFile<CR>

" Map editing and sourcing .vimrc
nmap <Leader>s :source $MYVIMRC
nmap <Leader>v :e! $MYVIMRC

" To save, ctrl-s.
nmap <c-s> :w<CR>
imap <c-s> <Esc>:w<CR>a

" NERDTree
map <F2> :NERDTreeToggle<CR> " Turns on/off NERDTree

" Personal keymaps
map <F5> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
      \ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
      \ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>

vmap <F6> :v/\S/d<CR> " Removes empty lines
vmap <F7> :rubydo $_ = $_.scan(/[A-Z]{2}_[A-Z]{4}_\d{6}[a-z]?[A-Z]{0,2}[_]?[0-9]?/).to_s<CR> " Scans for Audible product IDs
vmap <F8> :s/\n/, /g<CR> " Changes newlines to commas
nmap <Leader>g :rubydo $_ = $_.gsub(/
nmap <Leader>r :w! <bar> :Ruby %<CR>

set backup                   " Enable creation of backup file.
set backupdir=~/.vim/backups " Where backups will go.
set directory=~/.vim/tmp     " Where temporary files will go
